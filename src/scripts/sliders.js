
function showSlides(slideNumber, currentSlider) {
    let slides = currentSlider.querySelectorAll('.slider__item');

    if (slideNumber > slides.length) {
      slideNumber = 1;
    }

    if (slideNumber < 1) {
      slideNumber = slides.length;
    }

    for (var idx = 0; idx < slides.length; idx++) {
      slides[idx].style.display = "none";
    }

    slides[slideNumber - 1].style.display = "flex";
    currentSlider.dataset['id'] = slideNumber;
}

function reloadFrame(slideNumber, currentSlider) {
  let slides = currentSlider.querySelectorAll('.slider__item');
  let oldSlide = slides[slideNumber - 1];

  // reload prev slide
  let frame = oldSlide.querySelector('iframe');

  if (frame !== undefined) {
    frame.setAttribute('src', frame.getAttribute('src'));
  }
}

export function configureSliders() {
  let sliders = document.querySelectorAll('.slider');

  sliders.forEach(slider => {

    // slider clicks

    let prev = (e) => {
      let slideNumber = +slider.dataset['id'];
      showSlides(slideNumber - 1, slider);
      reloadFrame(slideNumber, slider);
    };

    let next = (e) => {
      let slideNumber = +slider.dataset['id'];
      showSlides(slideNumber + 1, slider);
      reloadFrame(slideNumber, slider);
    };

    // slider swiping

    let touchStart = (event) => {
      coordinatesX = event.touches[0].pageX;
    };

    let touchEnd = (event) => {
      let endCoordinates = event.changedTouches[0].pageX;
      if (coordinatesX > endCoordinates + 5) {
        // swipe to left
        next(event);
      } else if (coordinatesX < endCoordinates - 5) {
        // swipe to right
        prev(event);
      } else {
        let slideNumber = +slider.dataset['id'];
        let activeSlide = slider.querySelectorAll('.slider__item')[slideNumber - 1];
        let frame = activeSlide.querySelector('iframe');
        if (frame !== undefined) {
          frame.click();
        }
      }
    };

    // configure events

    var coordinatesX = 0;

    let previousControl = slider.querySelector('.slider__control-prev');
    let nextControl = slider.querySelector('.slider__control-next');

    previousControl.addEventListener('touchstart', touchStart);
    previousControl.addEventListener('touchend', touchEnd);
    previousControl.addEventListener('click', prev);

    nextControl.addEventListener('touchstart', touchStart);
    nextControl.addEventListener('touchend', touchEnd);
    nextControl.addEventListener('click', next);

    showSlides(+slider.dataset['id'], slider);
  });
}
