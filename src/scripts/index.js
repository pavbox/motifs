import '../styles/index.scss';

import { configureSliders } from './sliders.js';
import { configureMenu } from './menu.js';

function importAll(r) {
  return r.keys().map(r);
}

const images = importAll(require.context('./../img/', false, /\.(png|jpe?g|svg)$/));

document.addEventListener('DOMContentLoaded', (e) => {
  configureSliders();
  configureMenu();
});
