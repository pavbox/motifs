

function openMenu() {
  let menu = document.querySelector('.menu--mobile');
  menu.style.right = '0';
}

function closeMenu() {
  let menu = document.querySelector('.menu--mobile');
  menu.style.right = '-350px';
}

export function configureMenu() {
  let menu = document.querySelectorAll('.menu');

  menu.forEach((menu) => {
    let items = menu.querySelectorAll('.menu__item');

    items.forEach((menuItem) => {
      menuItem.addEventListener('click', (event) => {
        event.preventDefault();
        let linkName = event.target.innerText;
        let element = document.querySelector('#' + linkName);
        element.scrollIntoView({
          block: 'start',
          inline: 'nearest',
          behavior: 'smooth',
        });
        closeMenu();
      });
    });
  });

  let controls = document.querySelectorAll('.menu__control');

  controls.forEach(control => {
    let isCloseControl = control.classList.contains('menu--close');
    let event = isCloseControl ? closeMenu : openMenu;
    control.addEventListener('click', event);
  });
}
