
# once deployment script;
# remove repository, clone actual from master
# install dependencies and build project
# daemonize website

# @required git, npm and nodejs
# @recommended nodejs 8.6.0+

rm -rf * .* 2> /dev/null

source /home/motiirtp/nodevenv/home/motiirtp/motifs/9/bin/activate
git clone https://pavbox@bitbucket.org/pavbox/motifs.git .

npm install && npm run build
