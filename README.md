# motifs

Привет. Этот хостинг не предоставляет доступ к софту, поэтому наша сила в virtual envs.

Потребуется использовать как SSH подключение, так и File Manager из админки.


Для развертывания приложения:

1. Коннект через SSH
2. Активировать среду venv: `source /home/motiirtp/nodevenv/home/motiirtp/motifs/9/bin/activate`
3. Перейти к проекту: `cd /home/motiirtp/home/motiirtp/motifs`
4. Запустить сборку: `npm run build`
5. После сборки проект будет в папке `build/`. содержимое папки надо переместить в папку `/public_html`.

Сложности:

- Блочит `bin/node`: значит процесс уже занят. Нужно заюзать утилиту `top` и снять процессы NodeJS (`Node` or `Passenger Node`). Затем повторить операцию сборки.

Если надо загрузить из гитхаба, то `git clone https://pavbox@bitbucket.org/pavbox/motifs.git`

Для упрощения залил небольшой скрипт `deploy.sh`, который можно из консоли запускать.
